"""
Routes and views for the flask application.
"""

import os
import sqlite3
from datetime import datetime
from flask import render_template, Flask, request, session, g, redirect, url_for, abort, flash
from flask_googlemaps import GoogleMaps, Map
from PowerCycleFlask import app

app.config.from_object(__name__)

GoogleMaps(app)


app.config.update(dict(
    DATABSE = os.path.join(app.root_path, 'flaskr.db'),
    SECRET_KEY = 'development key',
    USERNAME = 'admin',
    PASSWORD = 'default'
    ))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

def connect_db():
    """Connects to the specific databse."""
    rv = sqlite3.connect(app.config['DATABASE'])
    rv.row_factory = sqlite3.Row
    return rv

def get_db():
    """Opens a new database connection if there is none yet for the current application context."""
    if not hasattr(g, 'sqlite_db'):
        g.sqlite_db = connect_db()
        return g.sqlite_db

@app.teardown_appcontext
def close_db(error):
    """Closes the database again at the end of the request."""
    if hasattr(g, 'sqlite_db'):
        g.sqlite_db.close()

def init_db() :
    db = get_db()
    with app.open_resource('schema.sql', mode='r') as f:
        db.cursor().executescript(f.read())
    db.commit()

@app.cli.command('initdb')
def initdb_command():
    """Initializes the database."""
    init_db()
    print('Initialized the database.')

@app.route('/')
def home():
    mymap = Map(
        identifier="decoupled-map",
        lat=37.4419,
        lng=-122.1419,
        markers=[(43.472262, -80.544868)]
    )
    print(app.root_path)
    if session.get('logged_in'):
        sndmap = Map(
            identifier="sndmap",
            lat=43.473551,
            lng=-80.527895,
            style=("height:20em;"
                "width:35em;"
                "float:left;"),
            zoom="12",
            markers=[
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.472262,
                 'lng': -80.544868,
                 'infobox': "<h4><b>3 bikes are available</b></h4><br />" + 
                            "<a class='btn btn-primary' href='station'> Book Now </a>'"
              },
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.473551,
                 'lng': -80.527895,
                 'infobox': "<h4><b>8 bikes are available</b></h4><br />" + 
                            "{% if not session.logged_in %}<a class='btn btn-primary'> Book Now </a>{% endif %}"
              },
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.447696,
                 'lng': -80.495666,
                 'infobox': "<h4><b>No bikes are available</b></h4>"
              }
            ]
        )
    else :
        sndmap = Map(
            identifier="sndmap",
            lat=43.473551,
            lng=-80.527895,
            style=("height:20em;"
                "width:35em;"),
            zoom="12",
            markers=[
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.472262,
                 'lng': -80.544868,
                 'infobox': "<h4><b>3 bikes are available</b></h4><br />"
              },
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.473551,
                 'lng': -80.527895,
                 'infobox': "<h4><b>8 bikes are available</b></h4><br />"
              },
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.447696,
                 'lng': -80.495666,
                 'infobox': "<h4><b>No bikes are available</b></h4>"
              }
            ]
        )
    return render_template('home.html', mymap=mymap, sndmap=sndmap)

@app.route('/account')
def account():
    if session.get('logged_in'):
        db = get_db()
        query = db.execute('select * from membership where userid = ?', (session['username'], ))
        entries = query.fetchall()
        return render_template('account.html', entries=entries) 
    else:
        return redirect(url_for('home'))

@app.route('/station')
def station():
    stationmap = Map(
            identifier="sndmap",
            lat=43.473551,
            lng=-80.527895,
            style=("height:20em;"
                "width:35em;"),
            zoom="12",
            markers=[
              {
                 'icon': 'http://maps.google.com/mapfiles/ms/icons/red-dot.png',
                 'lat': 43.472262,
                 'lng': -80.544868
              }
            ]
        )
    return render_template('station.html', stationmap=stationmap)

@app.route('/pickup')
def pickup():
    session['renting'] = True
    flash('Bike unlocked. Bike ID: 23EV158')
    return redirect(url_for('station'))

@app.route('/dropoff')
def dropoff():
    db = get_db()
    db.execute('update membership set points=points + 10, km= km + 1.3, time= time + 1 where userid = ?',( session.get('username'), ))
    db.commit()
    session ['renting'] = False
    flash('Please return to slot 3')
    return redirect(url_for('station'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    error = None
    db = get_db()
    if request.method == 'POST':
        query = db.execute('select userid from credentials')
        idList = query.fetchall()
        if request.form['username'] in idList:
            error='Username already in use'
        elif request.form['password1'] != request.form['password2']:
            error='Password do not match'
        else:
            db.execute('insert into credentials (userid, password) values (?, ?)', [request.form['username'], request.form['password1']])
            db.commit()
            db.execute('insert into membership (userid, points, km, time) values (?, 0, 0, 0)', (request.form['username'], ))
            db.commit()
            flash('Registration sucessful. Hello ' + request.form['username'])
            session['logged_in'] = True
            session['username'] = request.form['username']
            session['points'] = 0
            session['km'] = 0
            session['time'] = 0            
            return redirect(url_for('account'))
    return render_template('signup.html', error=error)
    

@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    db = get_db()
    if request.method == 'POST':
        username = request.form['username']
        # query = db.execute('select userid from credentials')
        # userList = query.fetchall()
        # if username not in userList:
        #     error = 'Invalid username'
        # else:
        query = db.execute('select password from credentials where userid = ?', (username, ))
        password = query.fetchone()[0]
        if request.form['password'] != password:
            error= 'Invlaid password '
        else:
            print("Credentials ok")
            query = db.execute('select DISTINCT * from membership where userid = ?', (username, ))
            data = query.fetchone()
            if data:
                session['logged_in'] = True
                session['username'] = data[0]
                session['points'] = data[1]
                session['km'] = data[2]
                session['time'] = data[3]
                flash('You were logged in')
                return redirect(url_for('account'))
    return render_template('login.html', error=error)

@app.route('/logogut')
def logout():
    session.pop('logged_in', None)
    session.pop('username', None)
    session.pop('points', None)
    session.pop('km', None)
    session.pop('time', None)
    flash('You were logged out')
    return redirect(url_for('home'))

@app.route('/users')
def users():
    db = get_db()
    cur = db.execute('select * from credentials')
    entries = cur.fetchall()
    return render_template('users.html', entries=entries)




#@app.route('/')
#@app.route('/home')
#def home():
#    """Renders the home page."""
#    return render_template(
#        'index.html',
#        title='Home Page',
#        year=datetime.now().year,
#    )

#@app.route('/contact')
#def contact():
#    """Renders the contact page."""
#    return render_template(
#        'contact.html',
#        title='Contact',
#        year=datetime.now().year,
#        message='Your contact page.'
#    )

#@app.route('/about')
#def about():
#    """Renders the about page."""
#    return render_template(
#        'about.html',
#        title='About',
#        year=datetime.now().year,
#        message='Your application description page.'
#    )

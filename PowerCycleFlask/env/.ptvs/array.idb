�}q (X   docqX  This module defines an object type which can efficiently represent
an array of basic values: characters, integers, floating point
numbers.  Arrays are sequence types and behave very much like lists,
except that the type of objects stored in them is constrained.
qX   membersq}q(X   __doc__q}q(X   kindqX   dataqX   valueq	}q
X   typeq]q(X   builtinsqX   strq�qX   __builtin__qX   strq�qesuX	   ArrayTypeq}q(hX   typerefqh	]qX   arrayqX   arrayq�qauX   __package__q}q(hhh	}qh]q(hhX   NoneTypeq�qesuX	   typecodesq }q!(hhh	}q"h]q#(hhesuX   _array_reconstructorq$}q%(hX   functionq&h	}q'(hX$   Internal. Used for pickling support.q(X	   overloadsq)]q*}q+(X   argsq,}q-(X   nameq.h,X
   arg_formatq/X   *q0u}q1(h.X   kwargsq2h/X   **q3u�q4hX$   Internal. Used for pickling support.q5uauuX   __name__q6}q7(hhh	}q8h]q9(hhesuX
   __loader__q:}q;(hhh	]q<X   _frozen_importlibq=X   BuiltinImporterq>�q?auh}q@(hhh	}qA(X   mroqB]qC(hhX   objectqD�qEeX   basesqF]qGhEahX5	  array(typecode [, initializer]) -> array

Return a new array whose items are restricted by typecode, and
initialized from the optional initializer value, which must be a list,
string or iterable over elements of the appropriate type.

Arrays represent basic values and behave very much like lists, except
the type of objects stored in them is constrained. The type is specified
at object creation time by using a type code, which is a single character.
The following type codes are defined:

    Type code   C Type             Minimum size in bytes 
    'b'         signed integer     1 
    'B'         unsigned integer   1 
    'u'         Unicode character  2 (see note) 
    'h'         signed integer     2 
    'H'         unsigned integer   2 
    'i'         signed integer     2 
    'I'         unsigned integer   2 
    'l'         signed integer     4 
    'L'         unsigned integer   4 
    'q'         signed integer     8 (see note) 
    'Q'         unsigned integer   8 (see note) 
    'f'         floating point     4 
    'd'         floating point     8 

NOTE: The 'u' typecode corresponds to Python's unicode character. On 
narrow builds this is 2-bytes on wide builds this is 4-bytes.

NOTE: The 'q' and 'Q' type codes are only available if the platform 
C compiler used to build Python supports 'long long', or, on Windows, 
'__int64'.

Methods:

append() -- append a new item to the end of the array
buffer_info() -- return information giving the current memory info
byteswap() -- byteswap all the items of the array
count() -- return number of occurrences of an object
extend() -- extend array by appending multiple elements from an iterable
fromfile() -- read items from a file object
fromlist() -- append items from the list
frombytes() -- append items from the string
index() -- return index of first occurrence of an object
insert() -- insert a new item into the array at a provided position
pop() -- remove and return item (default last)
remove() -- remove first occurrence of an object
reverse() -- reverse the order of the items in the array
tofile() -- write all items to a file object
tolist() -- return the array converted to an ordinary list
tobytes() -- return the array converted to a string

Attributes:

typecode -- the typecode character used to create the array
itemsize -- the length in bytes of one array item
qHh}qI(X   fromfileqJ}qK(hX   methodqLh	}qM(hXN   Read n objects from the file object f and append them to the end of the array.qNh)]qO(}qP(h,}qQ(h.h,h/h0u}qR(h.h2h/h3u�qShXN   Read n objects from the file object f and append them to the end of the array.qTu}qU(X   ret_typeqV]qWhaX   argsqX}qY(X   typeqZ]q[X   arrayq\X   arrayq]�q^aX   nameq_X   selfq`u}qa(hZ]qbhX   fileqc�qdah_X   fqeu}qf(hZ]qghX   intqh�qiah_X   nqju�qkueuuX   __contains__ql}qm(hhLh	}qn(hX   Return key in self.qoh)]qp(}qq(h,}qr(h.h,h/h0u}qs(h.h2h/h3u�qthX   Return key in self.quu}qv(hV]qwhX   boolqx�qyahX}qz(hZ]q{h^ah_h`u}q|(hZ]q}hX   objectq~�qah_X   valueq�u�q�ueuuX   extendq�}q�(hhLh	}q�(hX%   Append items to the end of the array.q�h)]q�(}q�(h,}q�(h.h,h/h0u}q�(h.h2h/h3u�q�hX%   Append items to the end of the array.q�u}q�(hV]q�hahX}q�(hZ]q�h^ah_h`u}q�(hZ]q�hah_X   iterableq�u�q�ueuuX   __ne__q�}q�(hhLh	}q�(hX   Return self!=value.q�h)]q�(}q�(h,}q�(h.h,h/h0u}q�(h.h2h/h3u�q�hX   Return self!=value.q�u}q�(hV]q�hahX}q�(hZ]q�hah_X   yq�u}q�(hZ]q�h^ah_X   xq�u�q�u}q�(hV]q�hahX}q�(hZ]q�h^ah_h�u}q�(hZ]q�hah_h�u�q�u}q�(hV]q�hyahX}q�(hZ]q�h^ah_h�u}q�(hZ]q�h^ah_h�u�q�ueuuX
   __sizeof__q�}q�(hhLh	}q�(hX&   Size of the array in memory, in bytes.q�h)]q�(}q�(h,}q�(h.h,h/h0u}q�(h.h2h/h3u�q�hX&   Size of the array in memory, in bytes.q�u}q�(hV]q�hiahX}q�(hZ]q�hah_X   selfq�u�q�ueuuX   insertq�}q�(hhLh	}q�(hX5   Insert a new item v into the array before position i.q�h)]q�(}q�(h,}q�(h.h,h/h0u}q�(h.h2h/h3u�q�hX5   Insert a new item v into the array before position i.q�u}q�(hV]q�hahX}q�(hZ]q�h^ah_h`u}q�(hZ]q�hiah_X   iq�u}q�(hZ]q�hah_h�u�q�ueuuX   __deepcopy__q�}q�(hhLh	}q�(hX   Return a copy of the array.q�h)]q�(}q�(h,}q�(h.h,h/h0u}q�(h.h2h/h3u�q�hX   Return a copy of the array.q�u}q�(hV]q�h^ahX}q�(hZ]q�h^ah_h`u�q�ueuuX   appendq�}q�(hhLh	}q�(hX+   Append new value v to the end of the array.q�h)]q�(}q�(h,}q�(h.h,h/h0u}q�(h.h2h/h3u�q�hX+   Append new value v to the end of the array.q�u}q�(hV]q�hahX}q�(hZ]q�h^ah_h`u}q�(hZ]q�hah_X   iterableq�u�q�ueuuX   __mul__q�}q�(hhLh	}q�(hX   Return self*value.nq�h)]q�(}q�(h,}r   (h.h,h/h0u}r  (h.h2h/h3u�r  hX   Return self*value.nr  u}r  (hV]r  h^ahX}r  (hZ]r  h^ah_X   arrayr  u}r	  (hZ]r
  hX   longr  �r  ah_X   valuer  u�r  u}r  (hV]r  h^ahX}r  (hZ]r  h^ah_X   arrayr  u}r  (hZ]r  hiah_X   valuer  u�r  ueuuX   __init_subclass__r  }r  (hh&h	}r  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r  h)]r  }r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r   hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r!  uauuX   fromlistr"  }r#  (hhLh	}r$  (hX    Append items to array from list.r%  h)]r&  (}r'  (h,}r(  (h.h,h/h0u}r)  (h.h2h/h3u�r*  hX    Append items to array from list.r+  u}r,  (hV]r-  hahX}r.  (hZ]r/  h^ah_h`u}r0  (hZ]r1  hah_X   iterabler2  u�r3  ueuuX   countr4  }r5  (hhLh	}r6  (hX/   Return number of occurrences of v in the array.r7  h)]r8  (}r9  (h,}r:  (h.h,h/h0u}r;  (h.h2h/h3u�r<  hX/   Return number of occurrences of v in the array.r=  u}r>  (hV]r?  hiahX}r@  (hZ]rA  h^ah_h`u}rB  (hZ]rC  hah_h�u�rD  ueuuX   indexrE  }rF  (hhLh	}rG  (hX3   Return index of first occurrence of v in the array.rH  h)]rI  (}rJ  (h,}rK  (h.h,h/h0u}rL  (h.h2h/h3u�rM  hX3   Return index of first occurrence of v in the array.rN  u}rO  (hV]rP  hiahX}rQ  (hZ]rR  h^ah_h`u}rS  (hZ]rT  hah_h�u�rU  ueuuX   __dir__rV  }rW  (hhLh	}rX  (hX.   __dir__() -> list
default dir() implementationrY  h)]rZ  }r[  (h,}r\  (h]r]  hX   objectr^  �r_  ah.X   selfr`  u�ra  hX   default dir() implementationrb  X   ret_typerc  ]rd  hX   listre  �rf  auauuX	   frombytesrg  }rh  (hhLh	}ri  (hX�   Appends items from the string, interpreting it as an array of machine values, as if it had been read from a file using the fromfile() method).rj  h)]rk  }rl  (h,}rm  (h.h,h/h0u}rn  (h.h2h/h3u�ro  hX�   Appends items from the string, interpreting it as an array of machine values, as if it had been read from a file using the fromfile() method).rp  uauuX   fromunicoderq  }rr  (hhLh	}rs  (hX�   Extends this array with data from the unicode string ustr.

The array must be a unicode type array; otherwise a ValueError is raised.
Use array.frombytes(ustr.encode(...)) to append Unicode data to an array of
some other type.rt  h)]ru  (}rv  (h,}rw  (h.h,h/h0u}rx  (h.h2h/h3u�ry  hX�   Extends this array with data from the unicode string ustr.

The array must be a unicode type array; otherwise a ValueError is raised.
Use array.frombytes(ustr.encode(...)) to append Unicode data to an array of
some other type.rz  u}r{  (hV]r|  hahX}r}  (hZ]r~  h^ah_h`u}r  (hZ]r�  hah_X   sr�  u�r�  ueuuX   __le__r�  }r�  (hhLh	}r�  (hX   Return self<=value.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self<=value.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_X   selfr�  u}r�  (hZ]r�  hah_X   otherr�  u�r�  ueuuX   __iter__r�  }r�  (hhLh	}r�  (hX   Implement iter(self).r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Implement iter(self).r�  uauuX   tobytesr�  }r�  (hhLh	}r�  (hXT   Convert the array to an array of machine values and return the bytes representation.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hXT   Convert the array to an array of machine values and return the bytes representation.r�  uauuX   __getitem__r�  }r�  (hhLh	}r�  (hX   Return self[key].r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self[key].r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  hX   slicer�  �r�  ah_X   indexr�  u�r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  hiah_X   indexr�  u�r�  ueuuX   itemsizer�  }r�  (hX   propertyr�  h	}r�  (hX%   the size, in bytes, of one array itemr�  h]r�  (hEhieuuX   remover�  }r�  (hhLh	}r�  (hX.   Remove the first occurrence of v in the array.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX.   Remove the first occurrence of v in the array.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  hah_X   valuer�  u�r�  ueuuX   tostringr�  }r�  (hhLh	}r�  (hX�   Convert the array to an array of machine values and return the bytes representation.

This method is deprecated. Use tobytes instead.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX�   Convert the array to an array of machine values and return the bytes representation.

This method is deprecated. Use tobytes instead.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX	   tounicoder�  }r�  (hhLh	}r�  (hX  Extends this array with data from the unicode string ustr.

Convert the array to a unicode string.  The array must be a unicode type array;
otherwise a ValueError is raised.  Use array.tobytes().decode() to obtain a
unicode string from an array of some other type.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX  Extends this array with data from the unicode string ustr.

Convert the array to a unicode string.  The array must be a unicode type array;
otherwise a ValueError is raised.  Use array.tobytes().decode() to obtain a
unicode string from an array of some other type.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX   __new__r�  }r�  (hh&h	}r�  (hXG   Create and return a new object.  See help(type) for accurate signature.r�  h)]r   (}r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hXG   Create and return a new object.  See help(type) for accurate signature.r  u}r  (hV]r  hahX}r  (hZ]r	  hX   typer
  �r  ah_X   typer  u}r  (X
   arg_formatr  h0hZ]r  hX   tupler  �r  ah_X   argsr  u�r  u}r  (hV]r  hahX}r  (hZ]r  j  ah_X   typer  u}r  (j  X   **r  hZ]r  hX   dictr  �r  ah_X   kwargsr  u}r  (j  h0hZ]r   j  ah_X   argsr!  u�r"  u}r#  (hV]r$  hahX}r%  (hZ]r&  j  ah_X   typer'  u}r(  (j  j  hZ]r)  j  ah_X   kwargsr*  u�r+  ueuuX   __rmul__r,  }r-  (hhLh	}r.  (hX   Return self*value.r/  h)]r0  (}r1  (h,}r2  (h.h,h/h0u}r3  (h.h2h/h3u�r4  hX   Return self*value.r5  u}r6  (hV]r7  h^ahX}r8  (hZ]r9  j  ah_X   valuer:  u}r;  (hZ]r<  h^ah_X   arrayr=  u�r>  u}r?  (hV]r@  h^ahX}rA  (hZ]rB  hiah_X   valuerC  u}rD  (hZ]rE  h^ah_X   arrayrF  u�rG  ueuuX   __ge__rH  }rI  (hhLh	}rJ  (hX   Return self>=value.rK  h)]rL  (}rM  (h,}rN  (h.h,h/h0u}rO  (h.h2h/h3u�rP  hX   Return self>=value.rQ  u}rR  (hV]rS  hahX}rT  (hZ]rU  h^ah_X   selfrV  u}rW  (hZ]rX  hah_X   otherrY  u�rZ  ueuuX   typecoder[  }r\  (hj�  h	}r]  (hX/   the typecode character used to create the arrayr^  h]r_  (hEheuuX   __hash__r`  }ra  (hhh	}rb  h]rc  (hX   NoneTyperd  �re  hesuX   __imul__rf  }rg  (hhLh	}rh  (hX   Implement self*=value.ri  h)]rj  (}rk  (h,}rl  (h.h,h/h0u}rm  (h.h2h/h3u�rn  hX   Implement self*=value.ro  u}rp  (hV]rq  h^ahX}rr  (hZ]rs  h^ah_h`u}rt  (hZ]ru  hiah_X   valuerv  u�rw  ueuuX   __copy__rx  }ry  (hhLh	}rz  (hX   Return a copy of the array.r{  h)]r|  (}r}  (h,}r~  (h.h,h/h0u}r  (h.h2h/h3u�r�  hX   Return a copy of the array.r�  u}r�  (hV]r�  h^ahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX   __str__r�  }r�  (hhLh	}r�  (hX   Return str(self).r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return str(self).r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  hah_X   or�  u�r�  ueuuX   byteswapr�  }r�  (hhLh	}r�  (hXx   Byteswap all items of the array.

If the items in the array are not 1, 2, 4, or 8 bytes in size, RuntimeError is
raised.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hXx   Byteswap all items of the array.

If the items in the array are not 1, 2, 4, or 8 bytes in size, RuntimeError is
raised.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX   __iadd__r�  }r�  (hhLh	}r�  (hX   Implement self+=value.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Implement self+=value.r�  u}r�  (hV]r�  h^ahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  h^ah_X   otherr�  u�r�  ueuuX   tofiler�  }r�  (hhLh	}r�  (hX9   Write all items (as machine values) to the file object f.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX9   Write all items (as machine values) to the file object f.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  hdah_heu�r�  ueuuX   __eq__r�  }r�  (hhLh	}r�  (hX   Return self==value.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self==value.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  hah_h�u}r�  (hZ]r�  h^ah_h�u�r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h�u}r�  (hZ]r�  hah_h�u�r�  u}r�  (hV]r�  hyahX}r�  (hZ]r�  h^ah_h�u}r�  (hZ]r�  h^ah_h�u�r�  ueuuX   __add__r�  }r�  (hhLh	}r�  (hX   Return self+value.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self+value.r�  u}r�  (hV]r�  h^ahX}r�  (hZ]r�  h^ah_X   selfr�  u}r�  (hZ]r�  h^ah_X   otherr�  u�r�  ueuuX   __lt__r�  }r�  (hhLh	}r�  (hX   Return self<value.r�  h)]r�  (}r   (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hX   Return self<value.r  u}r  (hV]r  hahX}r  (hZ]r  h^ah_X   selfr	  u}r
  (hZ]r  hah_X   otherr  u�r  ueuuX
   __reduce__r  }r  (hhLh	}r  (hX   helper for pickler  h)]r  (}r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hX   helper for pickler  u}r  (hV]r  j  ahX}r  (hZ]r  h^ah_h`u�r  ueuuX	   __class__r  }r  (hhh	]r  hX   typer   �r!  auh}r"  (hhh	}r#  h]r$  (hhesuX   __delitem__r%  }r&  (hhLh	}r'  (hX   Delete self[key].r(  h)]r)  (}r*  (h,}r+  (h.h,h/h0u}r,  (h.h2h/h3u�r-  hX   Delete self[key].r.  u}r/  (hV]r0  hahX}r1  (hZ]r2  h^ah_h`u}r3  (hZ]r4  j�  ah_X   slicer5  u�r6  u}r7  (hV]r8  hahX}r9  (hZ]r:  h^ah_h`u}r;  (hZ]r<  hiah_X   indexr=  u�r>  ueuuX   popr?  }r@  (hhLh	}rA  (hXH   Return the i-th element and delete it from the array.

i defaults to -1.rB  h)]rC  (}rD  (h,}rE  (h.h,h/h0u}rF  (h.h2h/h3u�rG  hXH   Return the i-th element and delete it from the array.

i defaults to -1.rH  u}rI  (hV]rJ  hahX}rK  (hZ]rL  h^ah_h`u}rM  (hZ]rN  hiah_h�u�rO  u}rP  (hV]rQ  hahX}rR  (hZ]rS  h^ah_h`u�rT  ueuuX   __repr__rU  }rV  (hhLh	}rW  (hX   Return repr(self).rX  h)]rY  (}rZ  (h,}r[  (h.h,h/h0u}r\  (h.h2h/h3u�r]  hX   Return repr(self).r^  u}r_  (hV]r`  hahX}ra  (hZ]rb  h^ah_h`u�rc  ueuuX   __delattr__rd  }re  (hhLh	}rf  (hX   Implement delattr(self, name).rg  h)]rh  (}ri  (h,}rj  (h.h,h/h0u}rk  (h.h2h/h3u�rl  hX   Implement delattr(self, name).rm  u}rn  (hV]ro  hahX}rp  (hZ]rq  hah_X   selfrr  u}rs  (hZ]rt  hah_X   nameru  u�rv  ueuuX   __len__rw  }rx  (hhLh	}ry  (hX   Return len(self).rz  h)]r{  (}r|  (h,}r}  (h.h,h/h0u}r~  (h.h2h/h3u�r  hX   Return len(self).r�  u}r�  (hV]r�  hiahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX   __subclasshook__r�  }r�  (hh&h	}r�  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r�  uauuX   __init__r�  }r�  (hhLh	}r�  (hX>   Initialize self.  See help(type(self)) for accurate signature.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX>   Initialize self.  See help(type(self)) for accurate signature.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  hah_X   selfr�  u}r�  (j  j  hZ]r�  j  ah_X   kwargsr�  u}r�  (j  h0hZ]r�  j  ah_X   argsr�  u�r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  hah_X   selfr�  u}r�  (j  h0hZ]r�  j  ah_X   argsr�  u�r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  hah_X   selfr�  u�r�  ueuuX   __setitem__r�  }r�  (hhLh	}r�  (hX   Set self[key] to value.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Set self[key] to value.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  j�  ah_X   indexr�  u}r�  (hZ]r�  hah_X   valuer�  u�r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u}r�  (hZ]r�  hiah_X   indexr�  u}r�  (hZ]r�  hah_X   valuer�  u�r�  ueuuX   tolistr�  }r�  (hhLh	}r�  (hX6   Convert array to an ordinary list with the same items.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX6   Convert array to an ordinary list with the same items.r�  u}r�  (hV]r�  hX   listr�  �r�  ahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX   reverser�  }r�  (hhLh	}r�  (hX,   Reverse the order of the items in the array.r�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX,   Reverse the order of the items in the array.r�  u}r�  (hV]r�  hahX}r�  (hZ]r�  h^ah_h`u�r�  ueuuX
   __format__r�  }r�  (hhLh	}r�  (hX   default object formatterr�  h)]r�  (}r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   default object formatterr�  u}r�  (hV]r   hahX}r  (hZ]r  hah_X   selfr  u}r  (hZ]r  hah_X
   formatSpecr  u�r  ueuuX   __reduce_ex__r  }r	  (hhLh	}r
  (hX&   Return state information for pickling.r  h)]r  (}r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hX&   Return state information for pickling.r  u}r  (hV]r  j  ahX}r  (hZ]r  h^ah_h`u�r  u}r  (hV]r  j  ahX}r  (hZ]r  h^ah_h`u}r  (hZ]r  hiah_X   versionr  u�r  ueuuX
   fromstringr  }r   (hhLh	}r!  (hX�   Appends items from the string, interpreting it as an array of machine values, as if it had been read from a file using the fromfile() method).

This method is deprecated. Use frombytes instead.r"  h)]r#  (}r$  (h,}r%  (h.h,h/h0u}r&  (h.h2h/h3u�r'  hX�   Appends items from the string, interpreting it as an array of machine values, as if it had been read from a file using the fromfile() method).

This method is deprecated. Use frombytes instead.r(  u}r)  (hV]r*  hahX}r+  (hZ]r,  h^ah_h`u}r-  (hZ]r.  hX   bufferr/  �r0  ah_X   bufr1  u�r2  u}r3  (hV]r4  hahX}r5  (hZ]r6  h^ah_h`u}r7  (hZ]r8  hah_j�  u�r9  u}r:  (hV]r;  hahX}r<  (hZ]r=  h^ah_h`u}r>  (hZ]r?  hX   bytesr@  �rA  ah_X   brB  u�rC  ueuuX   buffer_inforD  }rE  (hhLh	}rF  (hX�   Return a tuple (address, length) giving the current memory address and the length in items of the buffer used to hold array's contents.

The length should be multiplied by the itemsize attribute to calculate
the buffer length in bytes.rG  h)]rH  (}rI  (h,}rJ  (h.h,h/h0u}rK  (h.h2h/h3u�rL  hX�   Return a tuple (address, length) giving the current memory address and the length in items of the buffer used to hold array's contents.

The length should be multiplied by the itemsize attribute to calculate
the buffer length in bytes.rM  u}rN  (hV]rO  j  ahX}rP  (hZ]rQ  h^ah_h`u�rR  ueuuX   __setattr__rS  }rT  (hhLh	}rU  (hX%   Implement setattr(self, name, value).rV  h)]rW  (}rX  (h,}rY  (h.h,h/h0u}rZ  (h.h2h/h3u�r[  hX%   Implement setattr(self, name, value).r\  u}r]  (hV]r^  hahX}r_  (hZ]r`  hah_X   selfra  u}rb  (hZ]rc  hah_X   namerd  u}re  (hZ]rf  hah_X   valuerg  u�rh  ueuuX   __gt__ri  }rj  (hhLh	}rk  (hX   Return self>value.rl  h)]rm  (}rn  (h,}ro  (h.h,h/h0u}rp  (h.h2h/h3u�rq  hX   Return self>value.rr  u}rs  (hV]rt  hahX}ru  (hZ]rv  h^ah_X   selfrw  u}rx  (hZ]ry  hah_X   otherrz  u�r{  ueuuuuuX   __spec__r|  }r}  (hhh	}r~  h]r  h=X
   ModuleSpecr�  �r�  asuh>}r�  (hhh	}r�  (hB]r�  (h?hEehF]r�  hEahX�   Meta path import for built-in modules.

    All methods are either class or static methods to avoid the need to
    instantiate the class.

    r�  X	   is_hiddenr�  �h}r�  (h�}r�  (hhLh	}r�  (hX6   __sizeof__() -> int
size of object in memory, in bytesr�  h)]r�  }r�  (h,}r�  (h]r�  j_  ah.j`  u�r�  hX"   size of object in memory, in bytesr�  jc  ]r�  hX   intr�  �r�  auauuh�}r�  (hhLh	}r�  (hX   Return self!=value.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self!=value.r�  uauuj�  }r�  (hhLh	}r�  (hX   Return self==value.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self==value.r�  uauuX
   __module__r�  }r�  (hhh	}r�  h]r�  hasuj  }r�  (hh&h	}r�  (hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX�   This method is called when a class is subclassed.

The default implementation does nothing. It may be
overridden to extend subclasses.
r�  uauuj�  }r�  (hhLh	}r�  (hX   Return self<value.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self<value.r�  uauuj  }r�  (hhLh	}r�  (hX   helper for pickler�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   helper for pickler�  uauuX   __dict__r�  }r�  (hhh	}r�  h]r�  hX   mappingproxyr�  �r�  asuj  }r�  (hhh	]r�  j!  auh}r�  (hhh	}r�  h]r�  hasujV  }r�  (hhLh	}r�  (hX.   __dir__() -> list
default dir() implementationr�  h)]r�  }r�  (h,}r�  (h]r�  j_  ah.j`  u�r�  hX   default dir() implementationr�  jc  ]r�  jf  auauuX   __weakref__r�  }r�  (hj�  h	}r�  (hX2   list of weak references to the object (if defined)r�  h]r�  hEauuj�  }r�  (hhLh	}r�  (hX   Return self<=value.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self<=value.r�  uauuX   load_moduler�  }r�  (hh&h	}r�  (hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX�   Load the specified module into sys.modules and return it.

    This method is deprecated.  Use loader.exec_module instead.

    r�  uauuX   find_moduler�  }r�  (hh&h	}r�  (hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX�   Find the built-in module.

        If 'path' is ever specified then the search is considered a failure.

        This method is deprecated.  Use find_spec() instead.

        r�  uauujU  }r�  (hhLh	}r�  (hX   Return repr(self).r�  h)]r   }r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hX   Return repr(self).r  uauujd  }r  (hhLh	}r  (hX   Implement delattr(self, name).r  h)]r	  }r
  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hX   Implement delattr(self, name).r  uauuj�  }r  (hh&h	}r  (hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r  h)]r  }r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r  hX4  Abstract classes can override this to customize issubclass().

This is invoked early on by abc.ABCMeta.__subclasscheck__().
It should return True, False or NotImplemented.  If it returns
NotImplemented, the normal algorithm is used.  Otherwise, it
overrides the normal algorithm (and the outcome is cached).
r  uauuX   module_reprr  }r  (hh&h	}r  (hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r  h)]r  }r  (h,}r  (h.h,h/h0u}r  (h.h2h/h3u�r   hXs   Return repr for the module.

        The method is deprecated.  The import machinery does the job itself.

        r!  uauuj�  }r"  (hhLh	}r#  (hX>   Initialize self.  See help(type(self)) for accurate signature.r$  h)]r%  }r&  (h,}r'  (h.h,h/h0u}r(  (h.h2h/h3u�r)  hX>   Initialize self.  See help(type(self)) for accurate signature.r*  uauuX
   is_packager+  }r,  (hh&h	}r-  (hX4   Return False as built-in modules are never packages.r.  h)]r/  }r0  (h,}r1  (h.h,h/h0u}r2  (h.h2h/h3u�r3  hX4   Return False as built-in modules are never packages.r4  uauuj�  }r5  (hh&h	}r6  (hXG   Create and return a new object.  See help(type) for accurate signature.r7  h)]r8  }r9  (h,}r:  (h.h,h/h0u}r;  (h.h2h/h3u�r<  hXG   Create and return a new object.  See help(type) for accurate signature.r=  uauuX   create_moduler>  }r?  (hh&h	}r@  (hX   Create a built-in modulerA  h)]rB  }rC  (h,}rD  (h.h,h/h0u}rE  (h.h2h/h3u�rF  hX   Create a built-in modulerG  uauuj�  }rH  (hhLh	}rI  (hX   default object formatterrJ  h)]rK  }rL  (h,}rM  (h.h,h/h0u}rN  (h.h2h/h3u�rO  hX   default object formatterrP  uauuX   get_coderQ  }rR  (hh&h	}rS  (hX9   Return None as built-in modules do not have code objects.rT  h)]rU  }rV  (h,}rW  (h.h,h/h0u}rX  (h.h2h/h3u�rY  hX9   Return None as built-in modules do not have code objects.rZ  uauuX   exec_moduler[  }r\  (hh&h	}r]  (hX   Exec a built-in moduler^  h)]r_  }r`  (h,}ra  (h.h,h/h0u}rb  (h.h2h/h3u�rc  hX   Exec a built-in modulerd  uauuj  }re  (hhLh	}rf  (hX   helper for picklerg  h)]rh  }ri  (h,}rj  (h.h,h/h0u}rk  (h.h2h/h3u�rl  hX   helper for picklerm  uauujH  }rn  (hhLh	}ro  (hX   Return self>=value.rp  h)]rq  }rr  (h,}rs  (h.h,h/h0u}rt  (h.h2h/h3u�ru  hX   Return self>=value.rv  uauujS  }rw  (hhLh	}rx  (hX%   Implement setattr(self, name, value).ry  h)]rz  }r{  (h,}r|  (h.h,h/h0u}r}  (h.h2h/h3u�r~  hX%   Implement setattr(self, name, value).r  uauuji  }r�  (hhLh	}r�  (hX   Return self>value.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return self>value.r�  uauuj`  }r�  (hhLh	}r�  (hX   Return hash(self).r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return hash(self).r�  uauuX	   find_specr�  }r�  (hhh	}r�  h]r�  hX   methodr�  �r�  asuX
   get_sourcer�  }r�  (hh&h	}r�  (hX8   Return None as built-in modules do not have source code.r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX8   Return None as built-in modules do not have source code.r�  uauuj�  }r�  (hhLh	}r�  (hX   Return str(self).r�  h)]r�  }r�  (h,}r�  (h.h,h/h0u}r�  (h.h2h/h3u�r�  hX   Return str(self).r�  uauuuuuuu.